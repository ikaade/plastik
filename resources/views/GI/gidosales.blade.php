@extends('adminlte::page')

@section('title', 'Proses GI DO Sales')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@stop

@section('content_header')
    <h1>Proses GI DO Sales</h1>
@stop

@section('content')
<div class="col-md-6">
    <div class="box box-info">
            <div class="box-header with-border"></div>
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="plant" class="col-sm-4 control-label">Plant</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="plant" value="G39D" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="donumber" class="col-sm-4 control-label">Do Number</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="donumber">
                  </div>
                </div>
                <div class="form-group">
                  <label for="trucknumber" class="col-sm-4 control-label">Truck Number</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="trucknumber">
                  </div>
                </div>
                <div class="form-group">
                  <label for="drivername" class="col-sm-4 control-label">Driver Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="drivername">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="gidate" class="col-sm-4 control-label">GI Date</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="gidate" value="{{ Carbon\Carbon::today()->format('dmY') }}">
                  </div>
                </div>
               <div class="form-group">
                  <label for="storage" class="col-sm-4 control-label">Storage Location</label>

                  <div class="col-sm-8">
                    <select class="form-control">
                        <option>LSTK</option>
                        <option>WHSE</option>
                        <option>T001</option>
                        <option>T002</option>
                        <option>T003</option>
                        <option>T004</option>
                        <option>T005</option>
                        <option>T006</option>
                        <option>T007</option>
                        <option>T008</option>
                        <option>T009</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-default pull-right" style="margin-left: 5px;">Reset</button> 
                <button type="submit" class="btn btn-success pull-right" style="margin-left: 5px;">Proses</button> 
                <button type="submit" class="btn btn-info pull-right" style="margin-left: 5px;">Check DO</button>
              </div>
            </form>
          </div>
</div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script>
         $(function () {
                 $('#gidate').datepicker({
                    format: 'ddmmyyyy',
                    autoclose: true
                })
         })
    </script>
@stop