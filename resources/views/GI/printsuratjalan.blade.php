@extends('adminlte::page')

@section('title', 'Print Surat Jalan')

@section('content_header')
    <h1>Print Surat Jalan</h1>
@stop

@section('content')
<div class="col-md-6">
    <div class="box box-info">
            <div class="box-header with-border"></div>
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="plant" class="col-sm-4 control-label">Plant</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="plant" value="G39D" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="donumber" class="col-sm-4 control-label">Do Number</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="donumber">
                  </div>
                </div>
             <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right" style="margin-left: 5px;">Proses</button>
                <a for="plant" class="control-label pull-right">Berita_Acara.pdf    </a>
              </div>
            </form>
          </div>
</div>
@stop