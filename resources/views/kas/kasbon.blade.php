@extends('adminlte::page') 

@section('title', 'Plastik Wrap') 

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
<style>
.table {
  border: 0.5px solid #000000;
}
.table-bordered > thead > tr > th,
.table-bordered > tbody > tr > th,
.table-bordered > tfoot > tr > th,
.table-bordered > thead > tr > td,
.table-bordered > tbody > tr > td,
.table-bordered > tfoot > tr > td {
   border: 0.5px solid #000000;
}
</style>
@endsection 
@section('content_header')
<h1>Penggunaan Plastik Wrap</h1>
@endsection 
@section('content')
<div class="row">
@if(Session::has('info'))
<div class="col-md-12" id="info" name="info">
<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Lunas!</h4>
                {{ session('info') }}
</div>
</div>
@endif
@can('admin', \App\User::class)
<div class="col-md-4">
	<div class="box box-info">
			<form method="POST" action="/Kas/Kasbon" class="form-horizontal">
			{{ csrf_field() }}
		<div class="box-header">
			<h1 class="box-title">Input Kasbon</h1>
				<div class="pull-right box-tools">
					<button type="button" class="btn btn-info btn-sm" name="btnAdd" id="btnAdd"><i class="fa fa-plus"></i>&nbsp;&nbsp;Anggota</button>
              	</div>
		</div>
		<div class="box-body">
				<div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Nama :</label>
                  <div class="col-sm-8">
	                <select id="karyawan" name="karyawan" class="form-control">
					@foreach($karyawans as $karyawan)
						<option value="{{$karyawan->id}}">{{$karyawan->nama}} ({{$karyawan->jabatan}})</option>
					@endforeach
	                </select>
                  </div>
                </div>
				<div class="form-group">
                  <label for="tanggal" class="col-sm-4 control-label">Tanggal :</label>
                  <div class="col-sm-8">
	                <input type="text" name="tanggal" id="tanggal" class="form-control" value="{{Carbon\Carbon::now()->format('dmY')}}">
                  </div>
                </div>
				<div class="form-group">
                  <label for="mode" class="col-sm-4 control-label">Mode :</label>
                  <div class="col-sm-8">
					<div class="radio">
                    	<label>
                      	<input type="radio" name="mode" id="mode" value="kasbon" checked>
						Kasbon
                    	</label>
                  	</div>
					<div class="radio">
                    	<label>
                      	<input type="radio" name="mode" id="mode" value="pengembalian">
						Pengembalian
                    	</label>
                  	</div>
                  </div>
                </div>
				<div class="form-group">
                  <label for="nominal" class="col-sm-4 control-label">Nominal :</label>
                  <div class="col-sm-8">
					  <div class="input-group {{ $errors->has('nominal') ? 'has-error' : '' }}">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control text-right" id="nominal" name="nominal" placeholder="0">
                      <span class="input-group-addon">.00</span>
                      </div>
                  </div>
                </div>
				<div class="form-group">
                  <label for="keterangan" class="col-sm-4 control-label">Keterangan :</label>
                  <div class="col-sm-8">
					<textarea name="keterangan" id="keterangan" class="form-control" rows="3" placeholder="Keterangan"></textarea>
                  </div>
                </div>
		</div>
		<div class="box-footer">
        <div class="btn-group no-margin pull-right">
            <input type="reset" class="btn btn-warning btn-flat" value="Reset" />
            <input type="submit" class="btn btn-success btn-flat" value="Save" />
        </div>
		</div>
			</form>
	</div>
	<div class="box box-info" id="AddNew" name="AddNew" style="display: none;">
		<div class="box-header">
			<h1 class="box-title">Tambah Anggota</h1>
		</div>
			<form method="POST" action="/Kas/TambahAnggota" class="form-horizontal">
			{{ csrf_field() }}
		<div class="box-body">
				<div class="form-group">
                  <label for="nama" class="col-sm-3 control-label">Nama :</label>
                  <div class="col-sm-9">
	                <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama">
                  </div>
                </div>
				<div class="form-group">
                  <label for="jabatan" class="col-sm-3 control-label">Jabatan :</label>
                  <div class="col-sm-9">
	                <input type="text" name="jabatan" id="jabatan" class="form-control" placeholder="Jabatan">
                  </div>
                </div>
		</div>
		<div class="box-footer">
        <div class="btn-group no-margin pull-right">
            <input type="reset" class="btn btn-warning btn-flat" value="Reset" />
            <input type="submit" class="btn btn-success btn-flat" value="Save" />
        </div>
		</div>
			</form>
	</div>
</div>
@endcan
<div class="col-md-8">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h1 class="box-title">Daftar Kasbon Aktif</h1>
		</div>
		<div class="box-body">
			<div class="table-responsive-sm no-padding" style="overflow: auto; max-height: 500px;">
				<table class="table table-bordered">
					<thead>
						<tr class="success">
							<th class="text-center">Nama</th>
							<th class="text-center">Tanggal</th>
							<th class="text-center">Kasbon</th>
							<th class="text-center">Pengembalian</th>
							<th class="text-center">Keterangan</th>
							<th class="text-center">Sisa Kasbon</th>
						</tr>
					</thead>
					<tbody>
						@php
						$i = 0;
						$gtotal = 0; $gbagi = 0; $gsisa = 0;
						@endphp
						@foreach($kasbons as $kasbon)
							@php
							$i = $i + 1;
							$nama = $kasbon->karyawan->nama;
							$s = true;
							@endphp
							@foreach($kasbon->detail as $detail)
								<tr>
								@if($s)
								<td rowspan="{{count($kasbon->detail)}}"><b>{{$i}}. {{$nama}}</b></td>
								@endif
								<td class="text-center">@can('admin', \App\User::class) <a style="float:left;" class="btn btn-xs btn-danger" href="{{route('deletekasbon',['id' => $detail->id])}}">x</a> @endcan{{Carbon\Carbon::createFromFormat('dmY', $detail->tanggal)->format('d F Y')}}</td>
								<td class="text-right">{{$detail->keluar == null ? '':number_format($detail->keluar, 2, '.', ',')}}</td>
								<td class="text-right">{{$detail->masuk == null ? '':number_format($detail->masuk, 2, '.', ',')}}</td>
								<td>{{$detail->keterangan}}</td>
								@if($s)
								<td rowspan="{{count($kasbon->detail)}}" class="text-center">
								@can('admin', \App\User::class)
									<button type="button" class="btn btn-warning btn-sm" onclick="setKasbon({{$kasbon->karyawan->id}});"><i class="fa fa-print"></i> Input</button>
								@endcan
								</td>
								@php $s = false; @endphp									
								@endif
							</tr>
							@endforeach
							<tr class="info">
								<td colspan=2 class="text-center">Total</td>
								<td class="text-right"><b>{{number_format($kasbon->kasbon, 2, '.', ',')}}</b></td>
								<td class="text-right"><b>{{number_format($kasbon->pengembalian, 2, '.', ',')}}</b></td>
								<td colspan=2 class="text-right"><b>{{number_format($kasbon->sisa, 2, '.', ',')}}</b></td>
							</tr>
							@php
								$gtotal = $gtotal + $kasbon->kasbon; $gbagi = $gbagi + $kasbon->pengembalian; $gsisa = $gsisa + $kasbon->sisa;
							@endphp
						@endforeach
							<tr class="success">
								<td colspan=2 class="text-center">Grand Total</td>
								<td class="text-right"><b>{{number_format($gtotal, 2, '.', ',')}}</b></td>
								<td class="text-right"><b>{{number_format($gbagi, 2, '.', ',')}}</b></td>
								<td colspan=2 class="text-right"><b>{{number_format($gsisa, 2, '.', ',')}}</b></td>
							</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer"></div>

	</div>
</div>
</div>
@endsection 

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function() {
@if(Session::has('info'))
	setTimeout(function() {
    $('#info').hide(200);
		}, 3000);
@endif
	$(function () {
                 $('#tanggal').datepicker({
                    format: 'ddmmyyyy',
                    autoclose: true
                });
         });
	$("#btnAdd").click(function(){
    var $target = $('#AddNew'),
        $toggle = $(this);

    $target.slideToggle(100);
	});

});
	function setKasbon(id) {
  		$('#karyawan').val(id);
		$("input[name=mode][value='pengembalian']").prop("checked",true);
		$('#nominal').focus();
	}
</script>
@endsection