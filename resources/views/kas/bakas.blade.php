@extends('adminlte::page') 
@section('title', 'Print BA Kas') 
@section('content_header')
<h1>Print BA Kas</h1>
@stop 
@section('content')
<div class="row">
<div class="col-md-5">
    <div class="box box-info">
            <form class="form-horizontal" method="POST" action="{{route('postBAkas')}}">
            {{ csrf_field() }}
        <div class="box-header with-border"><h1 class="box-title">Print BA Kas</h1></div>
        <div class="box-body">
                <div class="form-group">
                    <label for="brankas" class="col-sm-4 control-label">Tunai (Brankas) :</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control text-right" id="brankas" name="brankas" placeholder="0" value="{{$kass[2]->nominal}}" />
                      <span class="input-group-addon">.00</span>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="brankas" class="col-sm-4 control-label">Saldo Rekening :</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control text-right" id="rekening" name="rekening" placeholder="0" value="{{$kass[7]->nominal}}" />
                      <span class="input-group-addon">.00</span>
                      </div>
                    </div>
                </div>
        </div>
        <div class="box-footer">
        <input type="submit" class="btn btn-success pull-right" name="submit" id="submit" value="Print" onclick="this.form.target='_blank';return true;" />
        <input type="submit" class="btn btn-info pull-right" name="submit" id="submit" value="Save" onclick="this.form.target='_self';return true;" />
        </div>
            </form>        
    </div>
</div>
<div class="col-md-6">
    <div class="box box-info">
        <div class="box-header with-border"><h1 class="box-title">Rincian Kas</h1></div>
        <div class="box-body">
			<div class="table-responsive no-padding">
				<table class="table table-striped">
					<thead>
						<tr class="danger">
							<th class="text-center">#</th>
							<th>Rincian</th>
							<th class="text-center">Nominal</th>
						</tr>
					</thead>
					<tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td>{{$kass[0]->desc}}</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format($kass[0]->nominal, 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td>{{$kass[1]->desc}}</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format($kass[1]->nominal, 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td>{{$kass[2]->desc}}</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format($kass[2]->nominal, 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td>{{$kass[3]->desc}}</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format($kass[3]->nominal, 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">5</td>
                            <td>{{$kass[7]->desc}}</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format($kass[7]->nominal, 2, '.', ',')}}</td>
                        </tr>
                        <tr class="info">
                            <td class="text-center"><b>6</b></td>
                            <td><b>Total Kas ( 2 + 3 + 4 + 5 )</b></td>
                            <td class="text-right" style="padding-right:20px"><b>Rp. {{number_format(($kass[1]->nominal+$kass[2]->nominal+$kass[3]->nominal+$kass[7]->nominal), 2, '.', ',')}}</b></td>
                        </tr>
                        <tr>
                            <td class="text-center">7</td>
                            <td>Total actual ( 1 + 2 + 3 + 4 + 5 )</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format(($kass[1]->nominal+$kass[2]->nominal+$kass[3]->nominal+$kass[7]->nominal+$kass[0]->nominal), 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">8</td>
                            <td>{{$kass[6]->desc}}</td>
                            <td class="text-right" style="padding-right:20px">Rp. {{number_format($kass[6]->nominal, 2, '.', ',')}}</td>
                        </tr>
                        <tr class="info">
                            <td class="text-center"><b>9</b></td>
                            <td><b>Selisih ( 7 - 8 )</b></td>
                            <td class="text-right" style="padding-right:20px"><b>Rp. {{number_format((($kass[1]->nominal+$kass[2]->nominal+$kass[3]->nominal+$kass[7]->nominal+$kass[0]->nominal)-$kass[6]->nominal), 2, '.', ',')}}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
        </div>
    </div>
</div>
</div>
@stop
