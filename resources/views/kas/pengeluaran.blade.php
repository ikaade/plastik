@extends('adminlte::page') 

@section('title', 'Plastik Wrap') 

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
<style>
.table {
  border: 0.5px solid #000000;
}
.table-bordered > thead > tr > th,
.table-bordered > tbody > tr > th,
.table-bordered > tfoot > tr > th,
.table-bordered > thead > tr > td,
.table-bordered > tbody > tr > td,
.table-bordered > tfoot > tr > td {
   border: 0.5px solid #000000;
}
</style>
@endsection 
@section('content_header')
<h1>Penggunaan Plastik Wrap</h1>
@endsection 
@section('content')
<div class="row">
@can('admin', \App\User::class)
<div class="col-md-4">
	<div class="box box-info">
			<form method="POST" action="/Kas/Pengeluaran" class="form-horizontal">
			{{ csrf_field() }}
		<div class="box-header">
			<h1 class="box-title">Input Pengeluaran</h1>
		</div>
		<div class="box-body">
				<div class="form-group">
                  <label for="tanggal" class="col-sm-4 control-label">Tanggal :</label>
                  <div class="col-sm-8">
	                <input type="text" name="tanggal" id="tanggal" class="form-control" value="{{Carbon\Carbon::now()->format('dmY')}}">
                  </div>
                </div>
				<div class="form-group">
                  <label for="nominal" class="col-sm-4 control-label">Nominal :</label>
                  <div class="col-sm-8">
					  <div class="input-group {{ $errors->has('nominal') ? 'has-error' : '' }}">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control text-right" id="nominal" name="nominal" placeholder="0">
                      <span class="input-group-addon">.00</span>
                      </div>
                  </div>
                </div>
				<div class="form-group">
                  <label for="keterangan" class="col-sm-4 control-label">Keterangan :</label>
                  <div class="col-sm-8 {{ $errors->has('keterangan') ? 'has-error' : '' }}">
					<textarea name="keterangan" id="keterangan" class="form-control" rows="3" placeholder="Keterangan"></textarea>
					 @if ($errors->has('keterangan'))
                        <span class="help-block">
                            <strong>Keterangan wajib di isi.</strong>
                        </span>
                    @endif
                  </div>
                </div>
		</div>
		<div class="box-footer">
        <div class="btn-group no-margin pull-right">
            <input type="reset" class="btn btn-warning btn-flat" value="Reset" />
            <input type="submit" class="btn btn-success btn-flat" value="Save" />
        </div>
		</div>
			</form>
	</div>
</div>
@endcan
<div class="col-md-8">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h1 class="box-title">Daftar Pengeluaran</h1>
		</div>
		<div class="box-body">
			<div class="table-responsive-sm no-padding" style="overflow: auto; max-height: 500px;">
				<table class="table table-bordered">
					<thead>
						<tr class="success">
							<th class="text-center">Tanggal</th>
							<th class="text-center">Keterangan</th>
							<th class="text-center">Nominal</th>
							<th class="text-center"></th>
						</tr>
					</thead>
					<tbody>
						@foreach($keluars as $k)
							<tr>
								<td class="text-center">{{Carbon\Carbon::createFromFormat('dmY', $k->tanggal)->format('d F Y')}}</td>
								<td class="text-center">{{$k->keterangan}}</b></td>
								<td class="text-center"><b>{{number_format($k->nominal, 2, '.', ',')}}</b></td>
								<td class="text-center"><a class="btn btn-sm btn-danger" href="/Kas/Pengeluaran/delete/{{$k->id}}" onclick="return confirm('Hapus {{$k->keterangan}} sebesar {{number_format($k->nominal, 2, '.', ',')}} ?');">Hapus</a></td>
							</tr>
						@endforeach
							<tr class="success">
								<td colspan=2 class="text-center"><b>Total Pengeluaran</b></td>
								<td class="text-center"><b>{{number_format($keluars->sum('nominal'), 2, '.', ',')}}</b></td>
								<td></td>
							</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer"></div>

	</div>
</div>
</div>
@endsection 

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function() {
@if(Session::has('info'))
	setTimeout(function() {
    $('#info').hide(200);
		}, 3000);
@endif
	$(function () {
                 $('#tanggal').datepicker({
                    format: 'ddmmyyyy',
                    autoclose: true
                });
         });
	$("#btnAdd").click(function(){
    var $target = $('#AddNew'),
        $toggle = $(this);

    $target.slideToggle(100);
	});

});
	function setKasbon(id) {
  		$('#karyawan').val(id);
		$('#nominal').focus();
	}
</script>
@endsection