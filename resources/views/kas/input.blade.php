@extends('adminlte::page')

@section('title', 'Input Tagihan')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-danger">
            <form class="form-horizontal" method="POST" action="{{route('inputtagihan')}}">
			{{ csrf_field() }}
			<div class="box-header with-border">
					@php 
                    $now = Carbon\Carbon::today()->format('Y'); 
                    $bln = Carbon\Carbon::today()->format('n'); 
                    @endphp
                <h1 class="box-title">Input Tagihan Bulan 
            		    <select name="bulan" id="bulan" onchange="GetTagihan();">
						@foreach($bulans as $bulan) 
						@if ($bln == $bulan['id'])
						<option value="{{$bulan['id']}}" selected>{{$bulan['name']}}</option>
						@else
						<option value="{{$bulan['id']}}">{{$bulan['name']}}</option>
						@endif 
						@endforeach
						</select>
				    	<select name="tahun" id="tahun" onchange="GetTagihan();">
                        @for ($i = 2012; $i<=$now; $i++)
                        @if ($i==$now) 
                        <option value="{{$i}}" selected>{{$i}}</option>
						@else
						<option value="{{$i}}">{{$i}}</option>
						@endif 
                        @endfor
						</select>
                </h1>
            </div>
            <div class="box-body">
                @foreach($agens as $agen) 
                <div class="form-group">
                  <label for="form!{{$agen['id']}}" class='col-sm-4 control-label'>{{$agen['nama']}} : </label>
                  <div class="col-sm-8">
                      <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control text-right" id="form!{{$agen['id']}}" name="form!{{$agen['id']}}" placeholder="0">
                      <span class="input-group-addon">.00</span>
                      </div>
                  </div>
                </div>
                @endforeach
                <hr />
                <div class="form-group">
                  <label for="bagi" class='col-sm-4 control-label'>DANA YANG TERBAGIKAN : </label>
                  <div class="col-sm-8">
                      <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control text-right" id="bagi" name="bagi" placeholder="0">
                      <span class="input-group-addon">.00</span>
                      </div>
                  </div>
                </div>
                <hr />
				<blockquote>
				<table><tr class="lead">
                <td style="padding-right:5px"><b>Total Tagihan</b></td><td style="padding-right:30px"><b>: Rp.</b></td>
                <td style="text-align: right;"><b id="totTagihan" name="totTagihan">0</b></td>
				</tr><tr class="lead">
                <td style="padding-right:5px"><b>Total Terbagikan</b></td><td style="padding-right:30px"><b>: Rp.</b></td>
                <td style="text-align: right"><b id="totBagi" name="totBagi">0</b></td>
				</tr><tr class="lead">
                <td style="padding-right:5px"><b>Total Sisa</b></td><td style="padding-right:30px"><b>: Rp.</b></td>
               	<td style="text-align: right"><b id="totSisa" name="totSisa">0</b></td>
	            </tr>
				<tr />
				</table>
				</blockquote>
            </div>
            <div class="box-footer clearfix">
            <div class="col-xs-4 col-sm-3 col-md-4">
                <div class="input-group input-group">
                    <span class="input-group-addon">Fee</span>
                    <input type="text" value="35" class="form-control" id="harga" name="harga" style="text-align: center" />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="GetData" name="GetData" ><i class='fa fa-refresh'></i></button>
                    </span>
                </div></div>
            <div class="col-xs-8 col-sm-9 col-md-8">
                <div class="btn-group no-margin pull-right">
                    <input type="reset" class="btn btn-warning btn-flat" value="Reset" />
                    <input type="submit" class="btn btn-success btn-flat" value="Save" />
                </div></div>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
      <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
      <script type = "text/javascript" language = "javascript">
        function GetTagihan() {
            $("#bagi").val('0');
            $('input[name*="form!"]').val('0');
            $.getJSON('/api/tagihan/'+$("#bulan").val()+'/'+$("#tahun").val(), function(jd) {
                $.each(jd.detail, function(ix, data){
					$('input[name="form!' + data.agen_id + '"]').val(data.nominal);
				});
                $("#bagi").val(jd.data[0].bagi);
                $("#totTagihan").html(numeral(sumform('input[name*="form!"]')).format('0,0.00'));
                $("#totBagi").html(numeral($("#bagi").val()).format('0,0.00'));
                $("#totSisa").html(numeral(sumform('input[name*="form!"]') - Number($("#bagi").val())).format('0,0.00'));
            });
        };

         $(document).ready(function() {
            GetTagihan();
            $("#GetData").click(function(event){
			   $(this).html("<i class='fa fa-spin fa-refresh'></i>");
               $.getJSON('/api/pw/'+$("#bulan").val()+'/'+$("#tahun").val(), function(jd) {
				$('input[name*="form!"]').val('0');
                $.each(jd.data, function(ix, data){
					$('input[name="form!' + data.agen_id + '"]').val(data.pemasangan * $('#harga').val());
				});
                $("#totTagihan").html(numeral(sumform('input[name*="form!"]')).format('0,0.00'));
                $("#totBagi").html(numeral($("#bagi").val()).format('0,0.00'));
                $("#totSisa").html(numeral(sumform('input[name*="form!"]') - Number($("#bagi").val())).format('0,0.00'));
                $("#GetData").html("<i class='fa fa-refresh'></i>");
               });
            });

            sumform = function(selector) {
                var sum = 0;
                $(selector).each(function() {
                    sum += Number($(this).val());
                });
                return sum;
            };

            $('input[name*="form!"]').keyup(function(event){
                $("#totTagihan").html(numeral(sumform('input[name*="form!"]')).format('0,0.00'));
                $("#totSisa").html(numeral(sumform('input[name*="form!"]') - Number($("#bagi").val())).format('0,0.00'));
            });

            $('input[name*="form!"]').change(function(event){
                $("#totTagihan").html(numeral(sumform('input[name*="form!"]')).format('0,0.00'));
                $("#totSisa").html(numeral(sumform('input[name*="form!"]') - Number($("#bagi").val())).format('0,0.00'));
            });

            $("#bagi").keyup(function(event){
                $("#totBagi").html(numeral($("#bagi").val()).format('0,0.00'));
                $("#totSisa").html(numeral(sumform('input[name*="form!"]') - Number($("#bagi").val())).format('0,0.00'));
            });

         });
		 
      </script>
@endsection