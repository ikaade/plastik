@extends('adminlte::page') 
@section('title', 'Print Per Agen') 
@section('content_header')
<h1>Proses Tagihan</h1>
@stop 
@section('content')
<div class="row">
<div class="col-md-6">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h1 class="box-title">Tagihan Belum Terbayar</h1>
		</div>
		<div class="box-body">
			<div class="table-responsive no-padding">
				<table class="table table-striped">
					<thead>
						<tr class="info">
							<th class="text-center">#</th>
							<th>AGEN</th>
							<th class="text-center">PERIODE</th>
							<th class="text-center">NOMINAL</th>
							<th class="text-center">#</th>
						</tr>
					</thead>
					<tbody>
					@php $i = 0; @endphp
                        @foreach($tagihans as $tagihan)
							@php $i++; $rp = "Rp. ".number_format($tagihan->nominal, 2, '.', ','); @endphp
                            <tr>
                                <td class="text-center">{{$i}}</td>
                                <td>{{$tagihan->agen->nama}}</td>
                                <td class="text-center">{{$tagihan->bulan->name}} {{$tagihan->tahun}}</td>
                                <td class="text-center">{{$rp}}</td>
                                <td class="text-center">
                                <a class="btn btn-sm btn-info" href="/print/pwagen/{{$tagihan->agen->id}}/{{$tagihan->bulan_id}}/{{$tagihan->tahun}}" target="_blank">Print</a>
                                <button class="btn btn-sm btn-success" onclick="doProses('{{$tagihan->agen->nama}}','{{$tagihan->agen->id}}','{{$tagihan->bulan->name}} {{$tagihan->tahun}}','{{$tagihan->bulan_id}}', '{{$tagihan->tahun}}','{{$rp}}')">Proses</button>
                                </td>
                            </tr>
                        @endforeach
						<tr class="info">
							<th class="text-center" colspan=3>TOTAL</th>
							<th class="text-center">Rp. {{number_format($total, 2, '.', ',')}}</th>
							<th class="text-center"></th>
						</tr>
                    </tbody>
                </table>
            </div>
		</div>
		<div class="box-footer">
                    <a class="btn btn-info btn-flat" href="{{route('tagihan')}}">List Tagihan</a>
                    <a class="btn btn-info btn-flat" href="{{route('inputview')}}">Input Tagihan</a>
		</div>
	</div>
</div>
<div class="col-md-4">
@if(Session::has('info'))
<div class="alert alert-success alert-dismissible" id="info" name="info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Lunas!</h4>
                {{ session('info') }}
</div>
@endif
	<div class="box box-danger">
			<form method=POST class="form-horizontal" action="/Kas/ProsesTagihan">
		<div class="box-header with-border">
			<h1 class="box-title">Proses</h1>
		</div>
		<div class="box-body">
			{{ csrf_field() }}
				<div class="form-group">
                  <label class="col-sm-4 control-label">Agen :</label>
                  <div class="col-sm-8 {{ $errors->has('agen_id') ? 'has-error' : '' }}">
	                <input type=text name=AgenName id=AgenName value="" class="form-control" disabled/>
	                <input type=hidden name=agen_id id=agen_id value="" />
                  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Periode :</label>
                  <div class="col-sm-8 {{ $errors->has('bulan_id') ? 'has-error' : '' }}">
	                <input type=text name=Periode id=Periode value="" class="form-control" disabled/>
	                <input type=hidden name=bulan_id id=bulan_id value="" />
	                <input type=hidden name=tahun id=tahun value="" />
                  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Nominal :</label>
                  <div class="col-sm-8 {{ $errors->has('agen_id') ? 'has-error' : '' }}">
	                <input type=text name=nom id=nom value="" class="form-control" disabled/>
                  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Pembayaran :</label>
                  <div class="col-sm-8 {{ $errors->has('nominal') ? 'has-error' : '' }}">
				  	<div class="input-group">
                    <span class="input-group-addon">Rp.</span>
	                <input type=text name=nominal id=nominal value="" class="form-control" placeholder="0" />
                    <span class="input-group-addon">.00</span>
					</div>
                  </div>
                </div>
		</div>
		<div class="box-footer">
		        <div class="btn-group no-margin pull-right">
                    <input type="reset" class="btn btn-warning btn-flat" value="Reset" />
                    <input type="submit" class="btn btn-success btn-flat" value="Proses" onclick="return confirm('Yakin ?');" />
                </div>
		</div>
			</form>
	</div>
</div>
</div>
@stop

@section('js')
      <script type = "text/javascript" language = "javascript">
         $(document).ready(function() {
@if(Session::has('info'))
	setTimeout(function() {
    $('#info').hide(200);
		}, 3000);
@endif
            $('#print').on('click', function(){
                $('<a href="/print/pwagen/'+$("#agen").val()+'/'+$("#bulan").val()+'/'+$("#tahun").val()+'" target="blank"></a>')[0].click();    
            })
         });
			function doProses(Nama, aid, Periode, bid, tid, nom){
				$('#AgenName').val(Nama);
				$('#agen_id').val(aid);
				$('#Periode').val(Periode);
				$('#bulan_id').val(bid);
				$('#tahun').val(tid);
				$('#nom').val(nom);
				$('#nominal').focus();				
			}
      </script>
@endsection