@extends('adminlte::page') 
@section('title', 'Tagihan Agen') 

@section('content_header')
<h1>Tagihan Agen</h1>
@endsection 
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h1 class="box-title">Tahun - </h1>
                <select onchange="location = this.value;">
				@for ($i = 2012; $i<=Carbon\Carbon::now()->format('Y'); $i++) 
                @if ($i==$thn) 
                    <option value="{{route('tagihan', ['thn' => $i])}}" selected>{{$i}}</option>
				@else
					<option value="{{route('tagihan', ['thn' => $i])}}">{{$i}}</option>
				@endif 
                @endfor
                </select>
				<div class="pull-right box-tools">
				@can('admin', \App\User::class)
				<a type="button" class="btn btn-success btn-sm" href="{{route('inputview')}}"><i class="fa fa-keyboard-o"></i> Input</a>
				@endcan
                <a type="button" class="btn btn-info btn-sm" href="{{route('printtagihan',['thn' => $thn])}}" target="_blank"><i class="fa fa-print"></i> Print</a>
              	</div>
			</div>
			<div class="box-body">
				<div class="table-responsive no-padding">
					<table class="table table-striped table-bordered">
						<thead>
							<tr class="info">
								<th>#</th>
								<th>AGEN</th>
								@foreach($bulans as $bulan)
								<th>{{$bulan['name']}}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@php $i = 0; @endphp 
                            @foreach($tagihans as $tagihan) 
                            @php $i++; @endphp
							<tr>
								<td>{{$i}}</td>
								<td>{{$tagihan['nama']}}</td>
								@foreach($bulans as $bulan)
								@php $xx = false; @endphp
									@foreach($tagihan['tagihan'] as $item) 
                                    @if($bulan['id'] == $item['bulan_id']) 
								@php $xx = true; @endphp
                                    @if($item['lunas_at'] == null) 
								<td style="text-align: right" class="danger">{{number_format($item['nominal'], 2, '.', ',')}}</td>
									@else 
								<td style="text-align: right">{{number_format($item['nominal'], 2, '.', ',')}}</td> 
                                    @endif 
                                    @endif 
                                    @endforeach
								@if($xx==false)
								<td style="text-align: right">
								</td>
								@endif
								@endforeach
							</tr>
							@endforeach
							</tbody>
							<tr class="success">
								<th colspan=2>Total</th>
								@foreach($bulans as $bulan)
								<th style="text-align: right">
									@foreach($bulanans as $bulanan) 
                                    @if($bulan['id'] == $bulanan['bulan_id']) 
                                        {{number_format($bulanan['nominal'], 2, '.', ',')}}
                                    @endif 
                                    @endforeach
								</th>							
								@endforeach
							</tr>
							<tr class="success">
								<th colspan=2>Terbagi</th>
								@foreach($bulans as $bulan)
								<th style="text-align: right">
									@foreach($bulanans as $bulanan) 
                                    @if($bulan['id'] == $bulanan['bulan_id']) 
                                        {{number_format($bulanan['bagi'], 2, '.', ',')}} 
                                    @endif 
                                    @endforeach
								</th>							
								@endforeach
							</tr>
							<tr class="success">
								<th colspan=2>Sisa</th>
								@foreach($bulans as $bulan)
								<th style="text-align: right">
									@foreach($bulanans as $bulanan) 
                                    @if($bulan['id'] == $bulanan['bulan_id']) 
                                        {{number_format($bulanan['sisa'], 2, '.', ',')}} 
                                    @endif 
                                    @endforeach
								</th>							
								@endforeach
							</tr>
					</table>
				</div>
                <h4>Berikut rincian data tagihan tahun {{$thn}} :</h4>
				<blockquote>
				<table><tr class="lead">
                <td style="padding-right:5px"><b>Total Tagihan</b></td><td style="padding-right:30px"><b>: Rp.</b></td>
                <td style="text-align: right;"><b>{{number_format($tahunan['nominal'], 2, '.', ',')}}</b></td>
				</tr><tr class="lead">
                <td style="padding-right:5px"><b>Total Terbagikan</b></td><td style="padding-right:30px"><b>: Rp.</b></td>
                <td style="text-align: right"><b>{{number_format($tahunan['bagi'], 2, '.', ',')}}</b></td>
				</tr><tr class="lead">
                <td style="padding-right:5px"><b>Total Sisa</b></td><td style="padding-right:30px"><b>: Rp.</b></td>
               	<td style="text-align: right"><b>{{number_format($tahunan['sisa'], 2, '.', ',')}}</b></td>
	            </tr>
				<tr />
				</table>
                <small>Update at @isset($tahunan->updated_at)
					{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $tahunan->updated_at->toDateTimeString())->format('d/m/Y')}}
				@endisset</small>
				</blockquote>
	    		</div>
			<div class="box-footer"></div>
		</div>
	</div>
</div>
@endsection
