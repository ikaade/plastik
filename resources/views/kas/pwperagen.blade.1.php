@extends('adminlte::page') 
@section('title', 'Print Per Agen') 
@section('content_header')
<h1>Print Tagihan</h1>
@stop 
@section('content')
<div class="row">
<div class="col-md-8">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h1 class="box-title">Agen - 
							<select name="agen" id="agen" onchange="location = this.value;">
							@foreach($agens as $agen) 
							@if ($id == $agen['id'])
							<option value="{{route('viewpwagen', ['id' => $agen['id'], 'thn' => $thn])}}" selected>{{$agen['nama']}}</option>
							@else
							<option value="{{route('viewpwagen', ['id' => $agen['id'], 'thn' => $thn])}}" >{{$agen['nama']}}</option>
							@endif 
							@endforeach
							</select>
						<select name="tahun" id="tahun" onchange="location = this.value;">
							@php 
                            $now = Carbon\Carbon::today()->format('Y'); 
                            @endphp 
                            @for ($i = 2015; $i<=$now; $i++)
                            @if ($i==$thn) 
							<option value="{{route('viewpwagen', ['id' => $id, 'thn' => $i])}}" selected>{{$i}}</option>
							@else
							<option value="{{route('viewpwagen', ['id' => $id, 'thn' => $i])}}">{{$i}}</option>
							@endif 
                            @endfor
						</select>
			</h1>
		</div>
		<div class="box-body">
			<div class="table-responsive no-padding">
				<table class="table table-striped">
					<thead>
						<tr class="info">
							<th class="text-center">AGEN</th>
							<th class="text-center">PERIODE</th>
							<th class="text-center">NOMINAL</th>
							<th class="text-center">LUNAS TANGGAL</th>
						</tr>
					</thead>
					<tbody>
                        @foreach($tagihans as $tagihan)
                            <tr>
                                <td class="text-center">{{$tagihan->agen->nama}}</td>
                                <td class="text-center">{{$tagihan->bulan->name}} {{$thn}}</td>
                                <td class="text-center">Rp. {{number_format($tagihan->nominal, 2, '.', ',')}}</td>
                                <td class="text-center">
                                @if($tagihan->lunas_at != null)
                                {{Carbon\Carbon::createFromFormat('dmY', $tagihan->lunas_at)->format('d F Y')}}
                                @else
                                <a class="btn btn-sm btn-info" href="/print/pwagen/{{$id}}/{{$tagihan->bulan_id}}/{{$thn}}" target="_blank">Print</a>
                                <a class="btn btn-sm btn-success" href="/Kas/Lunas/{{$id}}/{{$tagihan->bulan_id}}/{{$thn}}" onclick="return confirm('Tagihan {{$tagihan->agen->nama}} - {{$tagihan->bulan->name}} {{$thn}} sebesar Rp. {{number_format($tagihan->nominal, 2, '.', ',')}} Lunas?');">Lunas</a>
                                @endif
                                </td>
                            </tr>
                        @endforeach
						<tr class="info">
							<th class="text-center" colspan=2>TOTAL</th>
							<th class="text-center">Rp. {{number_format($total, 2, '.', ',')}}</th>
							<th class="text-center"></th>
						</tr>
                    </tbody>
                </table>
            </div>
		</div>
		<div class="box-footer"></div>

	</div>
</div>
@stop

@section('js')
      <script type = "text/javascript" language = "javascript">
         $(document).ready(function() {
            $('#print').on('click', function(){
                $('<a href="/print/pwagen/'+$("#agen").val()+'/'+$("#bulan").val()+'/'+$("#tahun").val()+'" target="blank"></a>')[0].click();    
            })
         });
      </script>
@endsection