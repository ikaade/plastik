@extends('adminlte::page') 
@section('title', 'Dashboard') 
@section('css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
@endsection 
@section('content_header')
<h1>Dashboard</h1>
@stop 
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-danger">
            <div class="box-header with-border"><h1 class="box-title">Tagihan Plastik Wrap 2017</h1></div> 
            <div class="box-body"><canvas id="canvas"></canvas></div>
            <div class="box-footer"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header"><h1 class="box-title">Review Kas</h1></div>
            <div class="box-body">
                  <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Rp. {{number_format($kass[6]->nominal, 2, '.', ',')}}</h3>

              <p>Total Pemasukan Kas</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
          </div>
                  <div class="progress-group">
                    <span class="progress-text">{{$kass[3]->desc}}</span>
                    <span class="progress-number"><b>Rp. {{number_format($kass[3]->nominal, 2, '.', ',')}}</b></span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: {{($kass[3]->nominal/$total)*100}}%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">{{$kass[4]->desc}}</span>
                    <span class="progress-number"><b>Rp. {{number_format($kass[4]->nominal, 2, '.', ',')}}</b></span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: {{($kass[4]->nominal/$total)*100}}%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">{{$kass[5]->desc}}</span>
                    <span class="progress-number"><b>Rp. {{number_format($kass[5]->nominal, 2, '.', ',')}}</b></span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-info" style="width: {{($kass[5]->nominal/$total)*100}}%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">{{$kass[6]->desc}}</span>
                    <span class="progress-number"><b>Rp. {{number_format($kass[6]->nominal, 2, '.', ',')}}</b></span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: {{($kass[6]->nominal/$total)*100}}%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">{{$kass[0]->desc}}</span>
                    <span class="progress-number"><b>Rp. {{number_format($kass[0]->nominal, 2, '.', ',')}}</b></span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{($kass[0]->nominal/$total)*100}}%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">{{$kass[1]->desc}}</span>
                    <span class="progress-number"><b>Rp. {{number_format($kass[1]->nominal, 2, '.', ',')}}</b></span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: {{($kass[1]->nominal/$total)*100}}%"></div>
                    </div>
                  </div>
             </div>
            <div class="box-footer"></div>
        </div>
    </div>
</div>
            
@stop
@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $.getJSON('/api/chart', function (json) {
            // will generate array with ['Monday', 'Tuesday', 'Wednesday']
            var labels = json.data.map(function (item) {
                return item.bulan.name;
            });

            var nominal = json.data.map(function (item) {
                return item.nominal;
            });

            var terbagi = json.data.map(function (item) {
                return item.bagi;
            });

            var sisa = json.data.map(function (item) {
                return item.sisa;
            });


            var data = {
                labels: labels,
                datasets: [
                    {
                        label: "Tagihan",   
                        backgroundColor: "rgba(123, 236, 80, .6)",
                        borderColor: "rgba(123, 236, 80, 1)",
                        data: nominal,
						fill: false,
                    },
                    {
                        label: "Terbagikan",
                        backgroundColor: "rgba(179,11,198,.6)",
                        borderColor: "rgba(179,11,198,1)",
                        data: terbagi,
						fill: false,
                    },
                    {
                        label: "Sisa",
                        backgroundColor: "rgba(92, 206, 247, .6)",
                        borderColor: "rgba(92, 206, 247, 1)",
                        data: sisa,
                    },

                ]
            };

            var ctx = canvas.getContext('2d');
            var config = {
                type: 'line',
                data: data,
				options: {
					scales: {
					yAxes: [{
						ticks: {
						// Include a dollar sign in the ticks
						callback: function(value, index, values) {
							return (value/1000000)+' jt';
						}
						}
						}]
					},
					tooltips: {
						mode: 'label',
            			callbacks: {
                			label: function(tooltipItems, data) {
                    			return data.datasets[tooltipItems.datasetIndex].label +': Rp. ' + numeral(tooltipItems.yLabel).format('0,0.00');
                			}
						},
            		},
				}
            };

            var chart = new Chart(ctx, config);
        });
    });
</script>
@endsection