@extends('print.portrait') 
@section('title', "PW") 
@section('content')
<img style="float: right;" src="http://opdaka.com/contents/images/opdaka-logo.png" />
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Semarang, {{Carbon\Carbon::now()->format('d F Y')}}</p>
<table>
    <tr>
        <td>No </td>
        <td> :</td>
    </tr>
    <tr>
        <td>Hal </td>
        <td> : Penagihan jasa pemasangan plastic wrap</td>
    </tr>
    <tr>
        <td>Lamp </td>
        <td> : 1 Lembar</td>
    </tr>
</table>
<p>&nbsp;</p>
<p style="margin: 0px">Kepada Yth</p>
<p style="margin: 0px">Pimpinan Agen {{$tagihan->agen->nama}}</p>
<p style="margin: 0px">Di tempat.</p>
<p>&nbsp;</p>
<p>Dengan hormat,</p>
<p>&nbsp;</p>
<p style="text-indent:45px;text-align:justify;">Sehubungan dengan telah terlaksananya proses pemasangan Plastic Wrap di Seal Caps tabung elpiji 3 kg,Maka dengan ini kami SPBE PT . OPTIMA DAYA KAPITAL bermaksud mengajukan penagihan jasa pemasangan Plastic Wrap tersebut dengan perincian data sebagai berikut :</p>
<div style="padding-left:75px;margin: 0px">
<table>
    <tr>
        <td>Periode tagihan	</td>
        <td> : BULAN {{$tagihan->bulan->name}} {{$tagihan->tahun}}</td>
    </tr>
    <tr>
        <td>Besaran tagihan </td>
        <td> : </td>
    </tr>
</table>
</div>
<table style="margin: auto;">
    <tr>
        <td style="border: 1px solid black;padding: 5px">Jasa Pemasangan PW :</td>
        <td style="border: 1px solid black;padding: 5px">{{$stok->keluar}} pcs = Rp. {{number_format($tagihan->nominal, 0, '.', ',')}}</td>
    </tr>
</table>
<p>&nbsp;</p>
<p style="text-indent:45px">Demikian kami sampaikan ,atas perhatian dan kerjsamanya kami ucapkan terima kasih.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="margin: 0px;text-indent:10px">Hormat Kami</p>
<p style="margin: 0px;text-indent:10px">SPBE PT . OPTIMA DAYA KAPITAL</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="margin: 0px;text-indent:10px;text-decoration: underline;"><b>Andi Juandi</b></p>
@endsection
