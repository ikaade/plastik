@extends('print.landscape') 
@section('title', "Tagihan Agen $thn") 
@section('content')

			<h4>Tagihan Plastik Wrap Tahun {{$thn}}</h4><div class="row" style="font-size: x-small"><div class="col-xs-12 table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th class="text-center">AGEN</th>
								@foreach($bulans as $bulan)
								<th class="text-center">{{$bulan['name']}}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@php $i = 0; @endphp 
                            @foreach($tagihans as $tagihan) 
                            @php $i++; @endphp
							<tr>
								<td class="text-center">{{$i}}</td>
								<td>{{$tagihan['nama']}}</td>
								@foreach($bulans as $bulan)
								<td style="text-align: right">
									@foreach($tagihan['tagihan'] as $item) 
                                    @if($bulan['id'] == $item['bulan_id']) 
                                        {{number_format($item['nominal'], 2, '.', ',')}} 
                                    @endif 
                                    @endforeach
								</td>
								@endforeach
							</tr>
							@endforeach
							<tr>
								<th colspan=2>Total</th>
								@foreach($bulans as $bulan)
								<th style="text-align: right">
									@foreach($bulanans as $bulanan) 
                                    @if($bulan['id'] == $bulanan['bulan_id']) 
                                        {{number_format($bulanan['nominal'], 2, '.', ',')}}
                                    @endif 
                                    @endforeach
								</th>							
								@endforeach
							</tr>
							<tr>
								<th colspan=2>Terbagi</th>
								@foreach($bulans as $bulan)
								<th style="text-align: right">
									@foreach($bulanans as $bulanan) 
                                    @if($bulan['id'] == $bulanan['bulan_id']) 
                                        {{number_format($bulanan['bagi'], 2, '.', ',')}} 
                                    @endif 
                                    @endforeach
								</th>							
								@endforeach
							</tr>
							<tr>
								<th colspan=2>Sisa</th>
								@foreach($bulans as $bulan)
								<th style="text-align: right">
									@foreach($bulanans as $bulanan) 
                                    @if($bulan['id'] == $bulanan['bulan_id']) 
                                        {{number_format($bulanan['sisa'], 2, '.', ',')}} 
                                    @endif 
                                    @endforeach
								</th>							
								@endforeach
							</tr>
						</tbody>
					</table>
				</div></div>
                <h6>Berikut rincian data tagihan tahun {{$thn}} :</h6>
				<blockquote style="font-size: small">
				<table><tr class="lead">
                <td style="padding-right:5px;font-size: small"><b>Total Tagihan</b></td><td style="padding-right:30px;font-size: small"><b>: Rp.</b></td>
                <td style="text-align: right;font-size: small"><b>{{number_format($tahunan['nominal'], 2, '.', ',')}}</b></td>
				</tr><tr class="lead">
                <td style="padding-right:5px;font-size: small"><b>Total Terbagikan</b></td><td style="padding-right:30px;font-size: small"><b>: Rp.</b></td>
                <td style="text-align: right;font-size: small"><b>{{number_format($tahunan['bagi'], 2, '.', ',')}}</b></td>
				</tr><tr class="lead">
                <td style="padding-right:5px;font-size: small"><b>Total Sisa</b></td><td style="padding-right:30px;font-size: small"><b>: Rp.</b></td>
               	<td style="text-align: right;font-size: small"><b>{{number_format($tahunan['sisa'], 2, '.', ',')}}</b></td>
	            </tr>
				<tr />
				</table>
                <small>Update at @isset($tahunan->updated_at)
					{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $tahunan->updated_at->toDateTimeString())->format('d/m/Y')}}
				@endisset</small>
				</blockquote>
				<small class="pull-right">Printed by {{$user['name']}} at {{Carbon\Carbon::now()->format('d/m/Y H:i')}} </small>


@endsection
