@extends('print.portrait') 
@section('title', "PW") 
@section('content')
<img style="float: right;" src="http://opdaka.com/contents/images/opdaka-logo.png" />
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 style="text-align: center;text-decoration: underline;">Rincian Pemasangan Plastik Wrap</h4>
<h5 style="text-align: center;text-decoration: underline;">PT. OPTIMA DAYA KAPITAL</h5>
<hr />
<p>Agen : <b>{{$agen['nama']}}</b></p>
<p>Periode : <b>{{$bulan}} {{$thn}}</b></p>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Tanggal</th>
							<th>Penerimaan</th>
							<th>Pemasangan</th>
							<th>Plat Nomor</th>
							<th>Stok</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						@php 
            $stok = 0; $masuk = 0; $keluar = 0; $tanggal = ""; $ttanggal = ""; 
            @endphp 
            @foreach($wraps as $wrap) 
            @php 
            $stok = ($stok + $wrap['masuk']) - $wrap['keluar']; 
            $masuk = ($masuk + $wrap['masuk']); 
            $keluar = ($keluar + $wrap['keluar']); 
            $tanggal = Carbon\Carbon::createFromFormat('dmY', $wrap['tanggal'])->format('d F Y'); 
            if($tanggal == $ttanggal){ 
              $tanggal = ""; 
            }else{ 
              $ttanggal = $tanggal; 
            } 
            if($wrap['masuk'] == 0){$wrap['masuk'] = "-";} 
            if($wrap['keluar'] == 0){ 
              $wrap['keluar'] = "-";
            } 
            $wrapid = $wrap['id']; 
            @endphp
						<tr>
							<td>{{$tanggal}}</td>
							<td>{{$wrap['masuk']}}</td>
							<td>{{$wrap['keluar']}}</td>
							<td>{{$wrap['plat']}}</td>
							<td>{{$stok}}</td>
							<td>{{$wrap['keterangan']}}</td>
						</tr>
						@endforeach
						<tr>
							<td colspan=7 />
						</tr>
						<tr>
							<th>Jumlah</th>
							<th>{{$masuk}}</th>
							<th>{{$keluar}}</th>
							<th></th>
							<th>{{$stok}}</th>
							<th></th>
						</tr>
					</tbody>
				</table>

@endsection