@extends('print.portrait') 
@section('title', "PW") 
@section('content')
<img style="float: left;" src="https://ardilamadi2.files.wordpress.com/2012/12/2b7f8-logoelpiji.png" height="102" width="75"/>
<img style="float: right;" src="http://opdaka.com/contents/images/opdaka-logo.png" />
<p>&nbsp;</p>
<h3 style="text-align: center;">Berita Acara</h3>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-indent:45px;text-align:justify;">Pada hari ini {{\App\Helpers\AppHelpers::exe()->namahari(Carbon\Carbon::now()->format('w'))}} tanggal {{\App\Helpers\AppHelpers::exe()->terbilang(Carbon\Carbon::now()->format('j'))}} bulan {{\App\Helpers\AppHelpers::exe()->namabulan(Carbon\Carbon::now()->format('n'))}} Tahun {{\App\Helpers\AppHelpers::exe()->terbilang(Carbon\Carbon::now()->format('Y'))}} telah dilakukan proses perhitungan dana kas dari hasil jasa pemasangan plastic wrap dari bulan Januari 2015 – {{\App\Helpers\AppHelpers::exe()->namabulan(Carbon\Carbon::now()->format('n'))}} {{Carbon\Carbon::now()->format('Y')}} dengan perincian sebagai berikut :</p>
<div>
<table style="margin:auto;">
    <tr>
        <td>Total Pendapatan : </td>
        <td style="padding-left:70px;">Januari 2015 - {{\App\Helpers\AppHelpers::exe()->namabulan(Carbon\Carbon::now()->format('n'))}} {{Carbon\Carbon::now()->format('Y')}} </td>
        <td style="padding-left:70px;"><b>Rp. {{number_format($kass[4]->nominal, 2, '.', ',')}}</b></td>
    </tr>
    <tr>
        <td>Total Yang Dibagikan : </td>
        <td style="padding-left:70px;">Januari 2015 - {{\App\Helpers\AppHelpers::exe()->namabulan(Carbon\Carbon::now()->format('n'))}} {{Carbon\Carbon::now()->format('Y')}} </td>
        <td style="padding-left:70px;"><b>Rp. {{number_format($kass[5]->nominal, 2, '.', ',')}}</b></td>
    </tr>
    <tr>
        <td>Total Pemasukan Kas : </td>
        <td style="padding-left:70px;">Januari 2015 - {{\App\Helpers\AppHelpers::exe()->namabulan(Carbon\Carbon::now()->format('n'))}} {{Carbon\Carbon::now()->format('Y')}} </td>
        <td style="padding-left:70px;"><b>Rp. {{number_format($kass[6]->nominal, 2, '.', ',')}}</b></td>
    </tr>
</table>
</div>
<p>&nbsp;</p>
<table style="margin: auto;">
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">1.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">{{$kass[0]->desc}}</td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format($kass[0]->nominal, 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">2.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">{{$kass[1]->desc}}</td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format($kass[1]->nominal, 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">3.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">{{$kass[2]->desc}}</td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format($kass[2]->nominal, 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">4.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">{{$kass[3]->desc}}</td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format($kass[3]->nominal, 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">5.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">{{$kass[7]->desc}}</td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format($kass[7]->nominal, 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;"><b>6.</b></td>
        <td style="border:1px solid black;width:200px;padding-left:20px;"><b>Total Kas ( 2+3+4+5 )</b></td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;"><b>Rp. {{number_format(($kass[1]->nominal+$kass[2]->nominal+$kass[3]->nominal+$kass[7]->nominal), 2, '.', ',')}}</b></td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">7.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">Total actual ( 1+2+3+4+5 ) </td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format(($kass[1]->nominal+$kass[2]->nominal+$kass[3]->nominal+$kass[7]->nominal+$kass[0]->nominal), 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;">8.</td>
        <td style="border:1px solid black;width:200px;padding-left:20px;">{{$kass[6]->desc}}</td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;">Rp. {{number_format($kass[6]->nominal, 2, '.', ',')}}</td>
    </tr>
    <tr>
        <td style="text-align:center;border:1px solid black;width:50px;"><b>9.</b></td>
        <td style="border:1px solid black;width:200px;padding-left:20px;"><b>Selisih ( 7 - 8 )</b></td>
        <td style="border:1px solid black;width:300px;"></td>
        <td style="text-align:right;border:1px solid black;padding-left:10px;padding-right:10px;"><b>Rp. {{number_format((($kass[1]->nominal+$kass[2]->nominal+$kass[3]->nominal+$kass[7]->nominal+$kass[0]->nominal)-$kass[6]->nominal), 2, '.', ',')}}</b></td>
    </tr>
</table>
<p>&nbsp;</p>
<p style="text-indent:45px">Demikian berita acara ini dibuat sebagai laporan dan agar dapat dipergunakan sebagaimana mestinya.</p>
<p>&nbsp;</p>
<p>Terima kasih.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="margin: 0px;text-indent:10px">Semarang, {{Carbon\Carbon::now()->format('j')}} {{\App\Helpers\AppHelpers::exe()->namabulan(Carbon\Carbon::now()->format('n'))}} {{Carbon\Carbon::now()->format('Y')}}</p>
<p>&nbsp;</p>
<table>
    <tr>
        <td style="padding-left:20px">Pemegang Kas</td>
        <td style="width:475px;"></td>
        <td>Mengetahui</td>
    </tr>
    <tr style="height:75px"></tr>
    <tr>
        <td style="padding-left:20px;text-decoration:underline;"><b>M. Andri Yuniarso</b></td>
        <td></td>
        <td style="text-decoration:underline;"><b>Arief Rakhmat</b></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Manager Finance</td>
    </tr>
</table>
@endsection
