<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
<style>
@page {
    size: 21cm 29.7cm;
    margin: 5mm 10mm 5mm 10mm; /* change the margins as you want them to be. */
}
body{
    width: 21cm;
    height: 29.7cm;
    margin: auto;
}
</style>
</head>
<body>
    @yield('content')
<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script type = "text/javascript" language = "javascript">
$(document).ready(function() {
  window.print();
});
</script>
</body>
</html>