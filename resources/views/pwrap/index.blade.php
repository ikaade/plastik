@extends('adminlte::page')

@section('title', 'Plastik Wrap')

@section('content_header')
    <h1>Penggunaan Plastik Wrap</h1>
@stop

@section('content')
<div class="col-md-6">
    <div class="box box-info">
            <div class="box-header with-border"></div>
              <div class="box-body">
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Agen</th>
        <th>Alamat</th>
        <th>Phone</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($datas as $data)
      <tr>
        <td>{{$data['nama']}}</td>
        <td>{{$data['alamat']}}</td>
        <td>{{$data['phone']}}</td>
        <td><a href="{{action('DataController@laporanpwagen', $data['id'])}}" class="btn btn-success">Lihat</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
                <div class="box-footer">
              </div>

  </div>
  </div>
@stop