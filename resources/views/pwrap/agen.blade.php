@extends('adminlte::page') 

@section('title', 'Plastik Wrap') 

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
<style>
.closePad { margin:auto; padding:1px; }
</style>
@endsection 
@section('content_header')
<h1>Penggunaan Plastik Wrap</h1>
@endsection 
@section('content')
<div class="row">
<div class="col-md-4">
	<div class="box box-info">
		<div class="box-header with-border"></div>
		<div class="box-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Agen</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($datas as $data)
					<tr>
						<td>{{$data['nama']}}</td>
						<td>
							<a href="{{action('DataController@laporanpwagen', $data['id'])}}" class="btn btn-success">Lihat</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="box-footer">
		</div>

	</div>
</div>
<div class="col-md-8">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h1 class="box-title">{{$agen['nama']}} - 
							<select name="bulan" id="bulan" onchange="location = this.value;">
							@foreach($bulans as $bulan) 
							@if ($bln['id'] == $bulan['id'])
							<option value="{{action('DataController@laporanpwagen', ['id' => $agen['id'],'bln' => $bulan['id'],'thn' => $thn])}}" selected>{{$bulan['name']}}</option>
							@else
							<option value="{{action('DataController@laporanpwagen', ['id' => $agen['id'],'bln' => $bulan['id'],'thn' => $thn])}}">{{$bulan['name']}}</option>
							@endif 
							@endforeach
							</select>
						<select name="tahun" id="tahun" onchange="location = this.value;">
							@php 
                $now = Carbon\Carbon::today()->format('Y'); 
              @endphp 
              @for ($i = 2012; $i<=$now; $i++)
              @if ($i==$thn) 
                <option value="{{action('DataController@laporanpwagen', ['id' => $agen['id'],'bln' => $bln['id'],'thn' => $i])}}" selected>{{$i}}</option>
								@else
								<option value="{{action('DataController@laporanpwagen', ['id' => $agen['id'],'bln' => $bln['id'],'thn' => $i])}}">{{$i}}</option>
							@endif 
              @endfor
						</select>
			</h1>
				<div class="pull-right box-tools">
					<a type="button" class="btn btn-info btn-sm" name="btnAdd" id="btnAdd" href="{{route('printpw',['id' => $agen['id'],'bln' => $bln['id'],'thn' => $thn])}}" target="_blank"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</a>
              	</div>
		</div>
		<div class="box-body">
			<div class="table-responsive no-padding" style="overflow: auto; max-height: 330px;">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Tanggal</th>
							<th>Penerimaan</th>
							<th>Pemasangan</th>
							<th>Plat Nomor</th>
							<th>Stok</th>
							<th>Keterangan</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@php 
            $stok = 0; $masuk = 0; $keluar = 0; $tanggal = ""; $ttanggal = ""; 
            @endphp 
            @foreach($wraps as $wrap) 
            @php 
            $stok = ($stok + $wrap['masuk']) - $wrap['keluar']; 
            $masuk = ($masuk + $wrap['masuk']); 
            $keluar = ($keluar + $wrap['keluar']); 
            $tanggal = Carbon\Carbon::createFromFormat('dmY', $wrap['tanggal'])->format('d F Y'); 
            if($tanggal == $ttanggal){ 
              $tanggal = ""; 
            }else{ 
              $ttanggal = $tanggal; 
            } 
            if($wrap['masuk'] == 0){$wrap['masuk'] = "-";} 
            if($wrap['keluar'] == 0){ 
              $wrap['keluar'] = "-";
            } 
            $wrapid = $wrap['id']; 
            @endphp
						<tr>
							<td>{{$tanggal}}</td>
							<td>{{$wrap['masuk']}}</td>
							<td>{{$wrap['keluar']}}</td>
							<td>{{$wrap['plat']}}</td>
							<td>{{$stok}}</td>
							<td>{{$wrap['keterangan']}}</td>
							<td>
							@can('staff', \App\User::class)
								<a href="{{route('deletepwagen',['wrapid' => $wrapid, 'id' => $wrap['agen_id'], 'bln' => $wrap['bulan'], 'thn' => $wrap['tahun']])}}" class="btn btn-danger" onclick="return confirm('Hapus {{$agen['nama']}} - {{Carbon\Carbon::createFromFormat('dmY', $wrap['tanggal'])->format('d F Y')}}?');">Hapus</a>
							@endcan
							</td>
						</tr>
						@endforeach
						<tr>
							<td colspan=7 />
						</tr>
						<tr>
							<th>Jumlah</th>
							<th>{{$masuk}}</th>
							<th>{{$keluar}}</th>
							<th></th>
							<th>{{$stok}}</th>
							<th></th>
							<th></th>
						</tr>
					</tbody>
				</table>
			</div>
							@can('staff', \App\User::class)
			<div>
				<hr />
				<h4>Input Data</h4>
			</div>
			<form method="POST" action="{{action('DataController@inputpwagen', $agen['id'])}}">
				{{ csrf_field() }}
				<div class="form-group col-md-2 closePad">
					<label for="tanggal" class="control-label">Tanggal</label>
					<input type="text" class="form-control" name="tanggal" id="tanggal" value="{{ Carbon\Carbon::today()->format('dmY') }}" />
				</div>
				<div class="form-group col-md-2 closePad">
					<label for="penerimaan" class="control-label">Penerimaan</label>
					<input type="text" class="form-control" name="masuk" id="penerimaan" value="" />
				</div>
				<div class="form-group col-md-2 closePad">
					<label for="pemasangan" class="control-label">Pemasangan</label>
					<input type="text" class="form-control" name="keluar" id="pemasangan" value="" />
				</div>
				<div class="form-group col-md-2 closePad">
					<label for="plat" class="control-label">Plat Nomor</label>
					<input type="text" class="form-control" name="plat" id="plat" value="" />
				</div>
				<div class="form-group col-md-3 closePad">
					<label for="ket" class="control-label">Keterangan</label>
					<input type="text" class="form-control" name="ket" id="ket" value="" />
				</div>
				<div class="form-group col-md-1 closePad">
					<label for="save" class="control-label">Action</label>
					<input type="submit" class="btn-sm btn-primary" id="save" name="save" value="Save" />
				</div>
			</form>
			@endcan
		</div>
		<div class="box-footer"></div>

	</div>
</div>
</div>
@endsection 

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
	$(function () {
                 $('#tanggal').datepicker({
                    format: 'ddmmyyyy',
                    autoclose: true
                })
         })
</script>
@endsection