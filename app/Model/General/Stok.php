<?php

namespace App\Model\General;
use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $fillable = ['agen_id', 'bulan_id', 'tahun'];
    //
    public function agen(){
        return $this->belongsTo('App\Model\General\Agen')->orderBy('nama','asc');
    }
}
