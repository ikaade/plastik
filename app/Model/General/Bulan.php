<?php

namespace App\Model\General;

use Illuminate\Database\Eloquent\Model;

class Bulan extends Model
{
    //
    public function tagihan()
    {
     return $this->hasMany('App\Model\Kas\Tagihan')->with('agen')->orderBy('bulan_id','asc')->orderby('tahun','asc');
    }
}
