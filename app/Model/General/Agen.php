<?php

namespace App\Model\General;

use Illuminate\Database\Eloquent\Model;

class Agen extends Model
{
    //
    public function PWrap()
    {
      return $this->hasMany('App\Model\Kas\PlastikWrap')->orderBy('tanggal','asc')->orderby('masuk','desc');
    }
    
    public function tagihan()
    {
      return $this->hasMany('App\Model\Kas\Tagihan')->orderBy('bulan_id','asc')->orderby('tahun','asc');
    }

 }
