<?php

namespace App\Model\Kas;

use Illuminate\Database\Eloquent\Model;

class Tahunan extends Model
{
    //
    protected $fillable = ['tahun'];
}
