<?php

namespace App\Model\Kas;

use Illuminate\Database\Eloquent\Model;

class Kasbon extends Model
{
    //
    protected $fillable = ['karyawan_id', 'lunas_at'];
    public function karyawan(){
        return $this->belongsTo('App\Model\General\Karyawan');
    }

    public function detail(){
        return $this->hasMany('App\Model\Kas\KasbonDetail')->orderBy('id','asc')->orderBy('keluar','desc');
    }
}
