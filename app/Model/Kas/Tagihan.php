<?php

namespace App\Model\Kas;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
    //
    protected $fillable = ['agen_id', 'bulan_id', 'tahun'];
    
    public function agen(){
        return $this->belongsTo('App\Model\General\Agen')->orderBy('nama','asc');
    }
    public function bulan(){
        return $this->belongsTo('App\Model\General\Bulan');
    }

}
