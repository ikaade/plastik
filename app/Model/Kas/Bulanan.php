<?php

namespace App\Model\Kas;

use Illuminate\Database\Eloquent\Model;

class Bulanan extends Model
{
    //
    protected $fillable = ['tahun','bulan_id'];
    
    public function bulan(){
        return $this->belongsTo('App\Model\General\Bulan');
    }

}
