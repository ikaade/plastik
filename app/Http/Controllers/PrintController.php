<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kas\Tagihan;
use App\Model\General\Bulan;
use App\Model\General\Agen;
use App\Model\Kas\Bulanan;
use App\Model\Kas\Tahunan;
use App\Model\Kas\Kasbon;
use App\Model\Kas\Kas;
use App\Model\Kas\KasbonDetail;
use \App\Model\General\Stok;
use \App\Model\General\Karyawan;

class PrintController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function printTagihan($thn = false, Request $request){
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        $this->thn = $thn;
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $bulans = Bulan::all();
        $tagihans = Agen::with(['tagihan' => function($q){
            $q->where('tahun', $this->thn);
        }])->get();
        $bulanans = Bulanan::with('bulan')->where('tahun', $thn)->get();
        $tahunan = Tahunan::where('tahun', $thn)->first();
        $user = $request->user();
        return view('print.tagihan',compact('bulans','thn','tagihans', 'bulanans', 'tahunan', 'user'));
    }

    public function printTagihanPerAgen($id, $bln = false, $thn = false,Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        if(!$bln){ $bln = \Carbon\Carbon::now()->format('n'); }
        $this->thn = $thn;
        $stok = Stok::firstOrCreate(['agen_id' => $id, 'bulan_id' => $bln, 'tahun' => $thn]);
        $tagihan = Tagihan::firstOrCreate(['agen_id' => $id, 'bulan_id' => $bln, 'tahun' => $thn])->load('agen','bulan');
        return view('print.pwperagen',compact('tagihan','stok'));
    }

    public function printPWPerAgen($id, $bln = false, $thn = false,Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        if(!$bln){ $bln = \Carbon\Carbon::now()->format('n'); }
        $agen = Agen::find($id)->toArray();
        $wraps = Agen::find($id)->PWrap->where('bulan',$bln)->where('tahun',$thn);
        $bulan = \App\Helpers\AppHelpers::exe()->namabulan($bln);
        return view('print.pwagen',compact('agen','wraps','bulan','thn'));
    }

    public function printBAkas(){
        $kass = Kas::all();
        return view('print.BAkas',compact('kass'));
    }
}
