<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Model\Kas\Tagihan;
use App\Model\General\Bulan;
use App\Model\General\Agen;
use App\Model\Kas\Bulanan;
use App\Model\Kas\Tahunan;
use App\Model\Kas\Kasbon;
use App\Model\Kas\Kas;
use App\Model\Kas\KasbonDetail;
use App\Model\Kas\Pengeluaran;
use \App\Model\General\Stok;
use \App\Model\General\Karyawan;

class KasController extends Controller
{
    private $thn = false;
    private $pdf;

    public function __construct()
    {
        $this->middleware('auth');
    }
    //

    public function tagihan($thn = false, Request $request){
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        $this->thn = $thn;
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);

        $bulans = Bulan::all();
        $tagihans = Agen::with(['tagihan' => function($q){
            $q->where('tahun', $this->thn);
        }])->get();

        $bulanans = Bulanan::with('bulan')->where('tahun', $thn)->get();

        $tahunan = Tahunan::where('tahun', $thn)->first();
        return view('kas.tagihan',compact('bulans','thn','tagihans', 'bulanans', 'tahunan'));
    }

    public function inputView($bln = false, $thn = false,Request $request){
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        if(!$bln){ $bln = \Carbon\Carbon::now()->format('n'); }
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $agens = Agen::all();
        $bulans = Bulan::all();
        
        return view('kas.input', compact('agens', 'bulans'));
    }

    public function inputPost(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $agens = Agen::all();
        $bulans = Bulan::all();
        $tot = 0;
        foreach ($agens as $agen) {
            $tagihan = Tagihan::firstOrCreate(['agen_id' => $agen->id, 'bulan_id' => $request->input('bulan'), 'tahun' => $request->input('tahun')]);
            $tagihan->nominal = $request->input('form!'. $agen->id);
            $tagihan->save();
            $tot = $tot + $request->input('form!'. $agen->id);
        }

        $bulanan = Bulanan::firstOrCreate(['tahun' => $request->input('tahun'),'bulan_id' => $request->input('bulan')]);
        $tmpnom = $bulanan->nominal;
        $tmpbagi = $bulanan->bagi;
        $bulanan->nominal = $tot;
        $bulanan->bagi = $request->input('bagi');
        $bulanan->sisa = $tot - $request->input('bagi');
        $bulanan->save();

        $tahunan = Tahunan::firstOrCreate(['tahun' => $request->input('tahun')]);
        $tahunan->nominal = ($tahunan->nominal - $tmpnom) + $tot;
        $tahunan->bagi = ($tahunan->bagi - $tmpbagi) + $request->input('bagi');
        $tahunan->sisa = $tahunan->nominal - $tahunan->bagi;
        $tahunan->save();

        $this->updateKas();
        return redirect()->route('tagihan', ['thn' => $request->input('tahun')]);
    }

    public function deleteKasbon($id, Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $p = KasbonDetail::find($id);
        $k = Kasbon::find($p->kasbon_id);
        $k->pengembalian = $k->pengembalian - $p->masuk;
        $k->kasbon = $k->kasbon - $p->keluar;
        $k->sisa = $k->kasbon - $k->pengembalian;
        $p->delete();
        $k->save();
        $this->updateKas();
        return redirect()->route('viewkasbon');
    }

    public function viewKasbon(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $kasbons = Kasbon::where('lunas_at', null)->with('detail')->with('karyawan')->get();
        $kasbons = $kasbons->sortBy('karyawan.nama');
        $karyawans = Karyawan::orderBy('nama','asc')->get();
        return view('kas.kasbon',compact('kasbons','karyawans'));
    }

    public function xviewTagihanPerAgen($id = false, $thn = false, Request $request){
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        if(!$id){ $id = 1; }
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $bulans = Bulan::all();
        $agens = Agen::all();
        $tagihans = Tagihan::with('bulan')->with('agen')->where('agen_id',$id)->where('tahun',$thn)->orderBy('bulan_id','asc')->get();
        $total = $tagihans->sum('nominal');

        return view('kas.pwperagen',compact('bulans','agens','thn','id','tagihans','total'));
    }

    public function prosesTagihan($id = false, $thn = false, Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $bulans = Bulan::all();
        $agens = Agen::all();
        $tagihans = Tagihan::with('bulan')->with('agen')->where('lunas_at',null)->get();
        $tagihans = $tagihans->sortBy('agen.nama');
        $total = $tagihans->sum('nominal');

        return view('kas.pwperagen',compact('bulans','agens','thn','id','tagihans','total'));
    }

    public function viewBAkas(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $kass = Kas::all();
        return view('kas.bakas',compact('kass'));
    }

    public function postBAkas(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $brankas = Kas::find(3);
        $brankas->nominal = $request->input('brankas');
        $brankas->save();
        $atm = Kas::find(8);
        $atm->nominal = $request->input('rekening');
        $atm->save();
        if($request->input('submit')=="Save"){
            return redirect()->route('viewBAkas');
        }else{
            return redirect()->route('printBAkas');
        }
    }

    public function viewPengeluaran(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $keluars = Pengeluaran::all();
        return view('kas.pengeluaran',compact('keluars'));
    }

    public function inputPengeluaran(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $request->validate([
            'keterangan' => 'required',
            'nominal' => 'required|numeric',
        ]);
        $p = new Pengeluaran();
        $p->tanggal = $request->input('tanggal');
        $p->keterangan = $request->input('keterangan');
        $p->nominal = $request->input('nominal');
        $p->save();
        
        $this->updateKas();
        return redirect()->route('viewpengeluaran');
    }

    public function deletePengeluaran($id, Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $p = Pengeluaran::find($id)->delete();
        $this->updateKas();
        return redirect()->route('viewpengeluaran');
    }

    public function inputKasbon(Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'admin']);
        $request->validate([
            'nominal' => 'required|numeric',
        ]);
        $kasbon = Kasbon::firstOrCreate(['karyawan_id' => $request->input('karyawan'), 'lunas_at' => null]);
        $k = new KasbonDetail();
        $k->kasbon_id = $kasbon->id;
        $k->tanggal = $request->input('tanggal');
        if ($request->input('mode')=="kasbon") {
            $k->keluar = $request->input('nominal');
            $kasbon->kasbon = $kasbon->kasbon + $request->input('nominal');
        } else {
            $k->masuk = $request->input('nominal');
            $kasbon->pengembalian = $kasbon->pengembalian + $request->input('nominal');
        }
        $k->keterangan = $request->input('keterangan');
        $k->save();
        $kasbon->sisa = $kasbon->kasbon - $kasbon->pengembalian;
        $kasbon->save();

        if ($kasbon->sisa <= 0) {
            $kasbon->lunas_at = \Carbon\Carbon::now()->format('dmY');
            $kasbon->save();
            $anggota = Karyawan::find($request->input('karyawan'));
            $info = "Kasbon ".$anggota->nama." (".$anggota->jabatan.") sebesar Rp. ".number_format($kasbon->kasbon, 2, '.', ',')." telah lunas.";
            $this->updateKas();
            return redirect()->route('viewkasbon')->with("info",$info);
        } else {
            $this->updateKas();
            return redirect()->route('viewkasbon');
        }
        
        return false;
    }

    public function lunasTagihan(Request $request){
        $request->validate([
            'agen_id' => 'required',
            'bulan_id' => 'required',
            'tahun' => 'required',
            'nominal' => 'required|numeric',
            ]);
        $t = Tagihan::where('agen_id',$request->input('agen_id'))->where('bulan_id',$request->input('bulan_id'))->where('tahun',$request->input('tahun'))->with('agen')->with('bulan')->first();
        $t->lunas_at = \Carbon\Carbon::now()->format('dmY');
        $t->nominal = $request->input('nominal');
        $t->save();
        $this->updateKas();
        $info = "Tagihan ".$t->agen->nama." periode ".$t->bulan->name." ".$t->tahun." sebesar Rp. ".number_format($t->nominal, 2, '.', ',')." telah lunas.";
        
        return redirect()->route('prosestagihan')->with("info",$info);
    }

    public function updateKas(){
        $k = Kas::find(1);
        $k->nominal = Pengeluaran::sum('nominal');
        $k->save();

        $k = Kas::find(2);
        $k->nominal = Kasbon::where('lunas_at',null)->sum('sisa');
        $k->save();

        $k = Kas::find(4);
        $k->nominal = Tagihan::where('lunas_at',null)->sum('nominal');
        $k->save();

        $x = Kas::find(5);
        $x->nominal = Tahunan::sum('nominal');
        $x->save();

        $x = Kas::find(6);
        $x->nominal = Tahunan::sum('bagi');
        $x->save();

        $x = Kas::find(7);
        $x->nominal = Tahunan::sum('sisa');
        $x->save();

        return false;
    }

}
