<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kas\Kas;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $kass = Kas::all();
        $total = $kass[4]->nominal;
        return view('dashboard',compact('kass','total'));
    }

    public function home(Request $request)
    {
        return view('home');
    }
}
