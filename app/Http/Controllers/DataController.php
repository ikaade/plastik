<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\General\Agen;
use \App\Model\Kas\PlastikWrap;
use \App\Model\General\Bulan;
use \App\Model\General\Stok;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function inputdata(Request $request)
    {
        $request->user()->authorizeRoles(['dev', 'manager', 'staff']);
        return view('input.harian');
    }

    public function laporanpw(Request $request)
    {
        $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
        $datas = Agen::all()->toArray();
        return view('pwrap.index', compact('datas'));
    }

    public function laporanpwagen($id, $bln = false, $thn = false, Request $request)
    {
        if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
        if(!$bln){ $bln = \Carbon\Carbon::now()->format('n'); }
       $request->user()->authorizeRoles(['dev', 'manager', 'staff', 'admin']);
       $wraps = Agen::find($id)->PWrap->where('bulan',$bln)->where('tahun',$thn);
       $datas = Agen::all()->toArray();
       $agen = Agen::find($id)->toArray();
       $bulans = Bulan::all()->toArray();
       $bln = Bulan::find($bln)->toArray();
       return view('pwrap.agen', compact('datas','wraps','agen','bulans','bln','thn'));
    }

    public function inputpwagen($id, $bln = false, $thn = false, Request $request)
    {
        $request->user()->authorizeRoles(['dev', 'manager', 'staff']);
        
        $bln = \Carbon\Carbon::createFromFormat('m', substr($request->input('tanggal'),2,2))->format('n');
        $thn = substr($request->input('tanggal'),-4);
        $add = new PlastikWrap();
        $add->agen_id = $id;
        $add->tanggal = $request->input('tanggal');
	    $add->masuk = $request->input('masuk');
		$add->bulan = $bln;
		$add->tahun = $thn;
        $add->keluar = $request->input('keluar');
        $add->plat = $request->input('plat');
        $add->keterangan = $request->input('ket');
        $add->save();

        $this->updateStok($id, $bln, $thn, $request->input('masuk'), $request->input('keluar'));

        return redirect()->route('laporanpwagen', ['id' => $id, 'bln' => $bln, 'thn' => $thn]);
    }

    public function deletepwagen($wrapid, $id, $bln, $thn, Request $request){
        $request->user()->authorizeRoles(['dev', 'manager', 'staff']);

        $agen = PlastikWrap::find($wrapid);
        $this->updateStok($id, $bln, $thn, $agen->masuk, $agen->keluar, true);
        $agen->delete();

        return redirect()->route('laporanpwagen', ['id' => $id, 'bln' => $bln, 'thn' => $thn]);
    }

    private function updateStok($agen, $bln, $thn, $in, $out, $r = false){
        $stok = Stok::firstOrCreate(['agen_id' => $agen, 'bulan_id' => $bln, 'tahun' => $thn]);
        if($r){
            $stok->masuk = ($stok->masuk - $in);
            $stok->keluar = ($stok->keluar - $out);
            $stok->stok = ($stok->stok - $in) + $out;
        }else{
            $stok->masuk = ($stok->masuk + $in);
            $stok->keluar = ($stok->keluar + $out);
            $stok->stok = ($stok->stok + $in) - $out;
        }
        $stok->save();
        return false;
    }
}
