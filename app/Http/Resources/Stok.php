<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Stok extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'agen_id' => $this->agen_id,
            'pemasangan' => $this->keluar,
            'agen' => $this->agen,
        ];
    }
}
