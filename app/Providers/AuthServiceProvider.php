<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('dev', function ($user) {
            if ($user->hasRole('dev')) {
                return true;
            }
            return false;
        });

        Gate::define('manager', function ($user) {
            if ($user->hasAnyRole(['manager', 'dev'])) {
                return true;
            }
            return false;
        });

        Gate::define('admin', function ($user) {
            if ($user->hasAnyRole(['admin', 'dev'])) {
                return true;
            }
            return false;
        });

        Gate::define('staff', function ($user) {
            if ($user->hasAnyRole(['staff', 'dev'])) {
                return true;
            }
            return false;
        });
    }
}
