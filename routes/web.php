<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});
Route::get('/home', function(){
    return redirect('/');
});

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/PlastikWrapAgen', 'DataController@laporanpw')->name('laporanpw');
Route::get('/PlastikWrapAgen/{id}/{bln?}/{thn?}', 'DataController@laporanpwagen')->name('laporanpwagen');
Route::post('/PlastikWrapAgen/{id}/{bln?}/{thn?}', 'DataController@inputpwagen')->name('inputpwagen');
Route::get('/PlastikWrapAgen/delete/{wrapid}/{id}/{bln}/{thn}', 'DataController@deletepwagen')->name('deletepwagen');

Route::get('/Kas/Tagihan/{thn?}', 'KasController@tagihan')->name('tagihan');

Route::get('/Kas/Input', 'KasController@inputView')->name('inputview');
Route::post('/Kas/Input', 'KasController@inputPost')->name('inputtagihan');
Route::get('/Kas/Kasbon', 'KasController@viewKasbon')->name('viewkasbon');
Route::post('/Kas/Kasbon', 'KasController@inputKasbon')->name('inputkasbon');
Route::get('/Kas/Kasbon/delete/{id}', 'KasController@deleteKasbon')->name('deletekasbon');
Route::get('/Kas/ProsesTagihan', 'KasController@prosesTagihan')->name('prosestagihan');
Route::get('/Kas/BAkas', 'KasController@viewBAkas')->name('viewBAkas');
Route::post('/Kas/BAkas', 'KasController@postBAkas')->name('postBAkas');
Route::get('/Kas/Pengeluaran', 'KasController@viewPengeluaran')->name('viewpengeluaran');
Route::get('/Kas/Pengeluaran/delete/{id}', 'KasController@deletePengeluaran')->name('deletepengeluaran');
Route::post('/Kas/Pengeluaran', 'KasController@inputPengeluaran')->name('inputpengeluaran');

Route::post('/Kas/ProsesTagihan', 'KasController@lunasTagihan')->name('lunastagihan');

Route::get('/print/tagihan/{thn?}', 'PrintController@printTagihan')->name('printtagihan');
Route::get('/print/pwagen/{id}/{bln?}/{thn?}', 'PrintController@printTagihanPerAgen')->name('printpwagen');
Route::get('/print/pw/{id}/{bln?}/{thn?}', 'PrintController@printPWPerAgen')->name('printpw');
Route::get('/print/BAkas', 'PrintController@printBAkas')->name('printBAkas');
