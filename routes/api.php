<?php

use Illuminate\Http\Request;
use App\Http\Resources\Stok as StokR;
use App\Http\Resources\Tagihan as TagihanR;
use App\Model\General\Stok;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/pw/{bln?}/{thn?}/', function ($bln = false, $thn = false) {
    if(!$bln){ $bln = \Carbon\Carbon::now()->format('n'); }
    if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
    return StokR::collection(Stok::with('agen')->where('bulan_id',$bln)->where('tahun',$thn)->get());
});

Route::get('/tagihan/{bln?}/{thn?}/', function ($bln = false, $thn = false) {
    if(!$bln){ $bln = \Carbon\Carbon::now()->format('n'); }
    if(!$thn){ $thn = \Carbon\Carbon::now()->format('Y'); }
    $data['data'] = TagihanR::collection(\App\Model\Kas\Bulanan::where('bulan_id',$bln)->where('tahun',$thn)->get());
    $data['detail'] = TagihanR::collection(\App\Model\Kas\Tagihan::where('bulan_id',$bln)->where('tahun',$thn)->get());
    return $data;
});

Route::get('/chart', function () {
    $thn = \Carbon\Carbon::now()->format('Y');
    return TagihanR::collection(\App\Model\Kas\Bulanan::with('bulan')->where('tahun',$thn)->get());
});

