<?php

use Illuminate\Database\Seeder;
use \App\Model\General\Bulan;

class BulanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $bulan = new Bulan();
        $bulan->name = 'JANUARI';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'FEBUARI';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'MARET';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'APRIL';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'MEI';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'JUNI';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'JULI';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'AGUSTUS';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'SEPTEMBER';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'OKTOBER';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'NOVEMBER';
        $bulan->save();
        $bulan = new Bulan();
        $bulan->name = 'DESEMBER';
        $bulan->save();
    }
}
