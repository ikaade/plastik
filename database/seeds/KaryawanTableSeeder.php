<?php

use Illuminate\Database\Seeder;
use App\Model\General\Karyawan;

class KaryawanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'operator' => [
                'Ade Irkham Widianto',
                'Abdul Somad',
                'Agum Gumelar',
                'Agung Suharyadi',
                'Arief Firman',
                'Fuad Sodikin',
                'Eva Wahyudi',
                'Hanung Subianto',
                'Iwan Ariyanto',
                'M. Nurrochman',
                'Sandy Adi Pramudya',
                'Suhartono',
                'Supardi',
                'Tri Widodo',
                'Maksum Hadi',
            ],
            'office' => [
                'M. Andri Yuniarso',
                'Andi Juandi',
                'Bambang Seto Adji',
                'Arief Rakhmat',
            ],
            'security' => [
                'Sigit Purnomo',
                'Slamet Santoso',
                'Suparyanto',
                'Hariyono',
            ],
            'other' => [
                'Faisal',
                'Hari Ariyanto',
                'Mbak nurul',
                'Kunaryanto',
                'Pak Evan',
                'Opdaka',
                ]
        ];

        foreach($data as $jbt => $namas){
            foreach($namas as $nama){
                $k = new Karyawan();
                $k->jabatan = $jbt;
                $k->nama = $nama;
                $k->save();
            }

        }
    }
}
