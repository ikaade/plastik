<?php

use Illuminate\Database\Seeder;
use App\Model\Kas\Kas;
use App\Model\Kas\Tahunan;

class KasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kas = [
            'Total Pengeluaran',
            'Total Kasbon',
            'Tunai ( Brankas )',
            'Non Tunai',
            'Total Pendapatan',
            'Total yang dibagikan',
            'Total Pemasukan Kas',
            'Saldo Rekening'
        ];

        foreach($kas as $k){
            $x = new Kas();
            $x->desc = $k;
            $x->save();
        }
        
        $x = Kas::find(5);
        $x->nominal = Tahunan::sum('nominal');
        $x->save();

        $x = Kas::find(6);
        $x->nominal = Tahunan::sum('bagi');
        $x->save();

        $x = Kas::find(7);
        $x->nominal = Tahunan::sum('sisa');
        $x->save();


}
}
