<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_employee = new Role();
        $role_employee->name = 'staff';
        $role_employee->description = 'A Staff User';
        $role_employee->save();
        $role_employee = new Role();
        $role_employee->name = 'admin';
        $role_employee->description = 'An Admin User';
        $role_employee->save();
        $role_manager = new Role();
        $role_manager->name = 'manager';
        $role_manager->description = 'A Manager User';
        $role_manager->save();
        $role_manager = new Role();
        $role_manager->name = 'dev';
        $role_manager->description = 'A Developer User';
        $role_manager->save();
    }
}
