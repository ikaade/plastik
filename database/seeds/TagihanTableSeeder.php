<?php

use Illuminate\Database\Seeder;
use App\Model\Kas\Tagihan;
use App\Model\Kas\Bulanan;
use App\Model\Kas\Tahunan;

class TagihanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $tagihan = json_decode(File::get("database/data/tagihan.json"));
        $terbagi = json_decode(File::get("database/data/terbagi.json"));
        foreach($tagihan as $dx){
            foreach($dx->data as $datay){
                for($ic='1';$ic<='12';$ic++){
                    if(isset($datay->$ic)){
                        $tagihan = new Tagihan();
                        $tagihan->agen_id = $datay->agen;
                        $tagihan->bulan_id =  $ic;
                        $tagihan->tahun = $dx->tahun;
                        $tagihan->lunas_at = "0510".$dx->tahun;
                        $tagihan->nominal = $datay->$ic;
                        $tagihan->save();
                    }
                }
            }
        }
        
        foreach($terbagi as $bagi){
            for($ic='1';$ic<='12';$ic++){
                if(isset($bagi->data->$ic)){
                    $sum = Tagihan::where('bulan_id',$ic)->where('tahun',$bagi->tahun)->sum('nominal');
                    $bulan = new Bulanan();
                    $bulan->bulan_id = $ic;
                    $bulan->tahun = $bagi->tahun;
                    $bulan->nominal = $sum;
                    $bulan->bagi = $bagi->data->$ic;
                    $bulan->sisa = $sum - $bagi->data->$ic;
                    $bulan->save();
                }
            }
        }

        for($ic='2015';$ic<='2017';$ic++){
            $tahun = new Tahunan();
            $tahun->tahun = $ic;
            $tahun->nominal = Bulanan::where('tahun',$ic)->sum('nominal');
            $tahun->bagi = Bulanan::where('tahun',$ic)->sum('bagi');
            $tahun->sisa = $tahun->nominal - $tahun->bagi;
            $tahun->save();
        }
    }

}
