<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_staff = Role::where('name', 'staff')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_manager  = Role::where('name', 'manager')->first();
        $role_dev  = Role::where('name', 'dev')->first();
        $employee = new User();
        $employee->name = 'Ade Shinichi';
        $employee->username = 'ade';
        $employee->email = 'ade@opdaka.com';
        $employee->password = bcrypt('aiw999');
        $employee->save();
        $employee->roles()->attach($role_dev);

        $employee = new User();
        $employee->name = 'M. Andri';
        $employee->username = 'andri';
        $employee->email = 'andri@opdaka.com';
        $employee->password = bcrypt('opdaka');
        $employee->save();
        $employee->roles()->attach($role_admin);

        $employee = new User();
        $employee->name = 'Andi J.';
        $employee->username = 'andi';
        $employee->email = 'andi@opdaka.com';
        $employee->password = bcrypt('opdaka');
        $employee->save();
        $employee->roles()->attach($role_staff);

        $employee = new User();
        $employee->name = 'Arief Rakhmat';
        $employee->username = 'arief';
        $employee->email = 'arief@opdaka.com';
        $employee->password = bcrypt('opdaka');
        $employee->save();
        $employee->roles()->attach($role_manager);
    }
}
