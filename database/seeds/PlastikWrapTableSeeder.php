<?php

use Illuminate\Database\Seeder;
use \App\Model\Kas\PlastikWrap;

class PlastikWrapTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '2';
        $employee->tanggal = '11112017';
        $employee->masuk = '5000';
        $employee->keluar = '0';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = 'Nganu';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '1';
        $employee->tanggal = '11112017';
        $employee->masuk = '5000';
        $employee->keluar = '0';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = 'Nganu';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '2';
        $employee->tanggal = '11112017';
        $employee->masuk = '5000';
        $employee->keluar = '0';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = 'Nganu';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '3';
        $employee->tanggal = '11112017';
        $employee->masuk = '5000';
        $employee->keluar = '0';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = 'Belum di kurangi yang rusak';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '3';
        $employee->tanggal = '12112017';
        $employee->masuk = '0';
        $employee->keluar = '560';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = '';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '3';
        $employee->tanggal = '12112017';
        $employee->masuk = '0';
        $employee->keluar = '560';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = '';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '3';
        $employee->tanggal = '13112017';
        $employee->masuk = '0';
        $employee->keluar = '360';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = '';
        $employee->save();
        //
        $employee = new PlastikWrap();
        $employee->agen_id = '3';
        $employee->tanggal = '14112017';
        $employee->masuk = '0';
        $employee->keluar = '360';
        $employee->plat = 'H 1323 UG';
        $employee->keterangan = '';
        $employee->save();
    }
}
