<?php

use Illuminate\Database\Seeder;
use \App\Model\General\Agen;

class AgenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $agen = new Agen();
        $agen->nama = 'PT. CAHYO SUMBER MIGAS';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. CANDI AGUNG PRATAMA';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. JULIAN PUTRA MANDIRI';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. KEMARA ANORAGA JYOTI';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. LAMORA PATRA JAYA';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. OTISA';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. SABDONO PUTRA';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
        $agen = new Agen();
        $agen->nama = 'PT. TARSO MIGAS JAYA';
        $agen->alamat = 'Alamat disini.';
        $agen->phone = '0123456789';
        $agen->save();
    }
}
