<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlastikWrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plastik_wraps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agen_id');
            $table->string('tanggal');
            $table->integer('bulan');
            $table->integer('tahun');
            $table->integer('masuk')->nullable()->default('0');
            $table->integer('keluar')->nullable()->default('0');
            $table->string('plat')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plastik_wraps');
    }
}
