<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBulanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulanans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bulan_id');
            $table->integer('tahun');
            $table->integer('nominal')->nullable()->default('0');
            $table->integer('bagi')->nullable()->default('0');
            $table->integer('sisa')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulanans');
    }
}
